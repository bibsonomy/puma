# Academic Publication Management

PUMA stands for an integrated solution, where the upload of a publication results automatically in an update of the researchers private and institutional homepage, the academic reporting system, and in the institutional repository. Furthermore, lecturers can use PUMA in order to extend eLearning with publication management functions.
Find more information on [http://www.academic-puma.de/](http://www.academic-puma.de/).

# Akademisches Publikationsmanagement

PUMA steht für eine integrierte Lösung, welche das Einstellen einer eigenen Publikation dahingehend automatisiert, dass mit diesem einen Schritt auch die privaten und Institutshomepages, die akademische Reporting-Systeme und der Dokumentenserver aktualisiert werden können. Darüber hinaus können Dozenten PUMA verwenden, um die e-Learning-Systeme mit Publikation-Management-Funktionen zu erweitern.

Mehr Informationen finden Sie unter: [http://www.academic-puma.de/](http://www.academic-puma.de/).