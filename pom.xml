<!--

    PUMA - Academic PUblication MAnagement

    Copyright (C) 2009 - 2016 Knowledge & Data Engineering Group,
                                  University of Kassel, Germany
                                  http://www.kde.cs.uni-kassel.de/
                              Data Mining and Information Retrieval Group,
                                  University of Würzburg, Germany
                                  http://www.is.informatik.uni-wuerzburg.de/en/dmir/
                              L3S Research Center,
                                  Leibniz University Hannover, Germany
                                  http://www.l3s.de/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>de.academic-puma</groupId>
	<artifactId>puma</artifactId>
	<version>4.2.10</version>
	<packaging>pom</packaging>
	<name>PUMA</name>
	<description>Academic PUblication MAnagement</description>
	<inceptionYear>2009</inceptionYear>
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<maven.tomcat.path>/bibsonomy-webapp</maven.tomcat.path>
	</properties>
	<repositories>

		<repository>
			<id>bibsonomy.local</id>
			<name>dev.bibsonomy.org-repo</name>
			<url>http://dev.bibsonomy.org/maven2</url>
<!--			<url>/home/mho/Repositories/bibsonomy/</url>-->
<!--			<url>/home/kchoong/Work/repos/bibsonomy/</url>-->
			<snapshots>
				<updatePolicy>always</updatePolicy>
			</snapshots>
		</repository>
		
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>dev.bibsonomy.org-plugin</id>
			<name>Official Source Code Releases of selected BibSonomy Plugins</name>
			<url>https://dev.bibsonomy.org/maven2/</url>
			<snapshots>
				<updatePolicy>always</updatePolicy>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>
	
	<dependencies>
		<dependency>
			<groupId>org.bibsonomy</groupId>
			<artifactId>bibsonomy-webapp</artifactId>
			<version>${project.version}</version>
			<type>war</type>
		</dependency>
	</dependencies>

	<modules>
		<module>bibsonomy-webapp-puma</module>
	</modules>
	
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.tomcat.maven</groupId>
				<artifactId>tomcat7-maven-plugin</artifactId>
				<version>2.2</version>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.apache.maven.plugins
										</groupId>
										<artifactId>
											maven-dependency-plugin
										</artifactId>
										<versionRange>
											[2.8,)
										</versionRange>
										<goals>
											<goal>unpack</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.apache.maven.plugins
										</groupId>
										<artifactId>
											maven-antrun-plugin
										</artifactId>
										<versionRange>
											[1.4,)
										</versionRange>
										<goals>
											<goal>run</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-dependency-plugin</artifactId>
					<version>2.8</version>
					<executions>
						<!-- Extract less files for compiling -->
						<execution>
							<id>unpack-less-files</id>
							<phase>process-sources</phase>
							<goals>
								<goal>unpack</goal>
							</goals>
							<configuration>
								<artifactItems>
									<artifactItem>
										<groupId>org.bibsonomy</groupId>
										<artifactId>bibsonomy-webapp</artifactId>
										<version>${project.version}</version>
										<type>war</type>
										<overWrite>true</overWrite>
										<outputDirectory>${project.build.directory}/less-work</outputDirectory>
										<includes>resources/bootstrap/less/**/*.less,resources/css/**/*.less,resources/fonts/font-awesome/less/*.less,resources/jquery/plugins/ui/jquery-ui.css,resources/star-rating/star-rating.css,resources/select2/css/select2.css</includes>
									</artifactItem>
								</artifactItems>
							</configuration>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<artifactId>maven-antrun-plugin</artifactId>
					<version>1.4</version>
					<executions>
						<execution>
							<phase>process-sources</phase>
							<configuration>
								<tasks>
									<copy overwrite="true" todir="${project.build.directory}/less-work/resources">
										 <fileset dir="${project.basedir}/src/main/webapp/resources" includes="**/*" />
									</copy>
								</tasks>
							</configuration>
							<goals>
								<goal>run</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.lesscss</groupId>
					<artifactId>lesscss-maven-plugin</artifactId>
					<version>1.7.0.1.1</version>
					<configuration>
						<sourceDirectory>${project.build.directory}/less-work/resources/css</sourceDirectory>
						<outputDirectory>${project.build.directory}/less-out/resources/css</outputDirectory>
						<compress>true</compress>
						<includes>
							<include>bootstrap-style.less</include>
							<include>csl.less</include>
						</includes>
					</configuration>
					<executions>
						<execution>
							<goals>
								<goal>compile</goal>
							</goals>
						</execution>
					</executions>
					<dependencies>
						<!-- this is our version of less css java which supports inline css -->
						<dependency>
							<groupId>org.lesscss</groupId>
							<artifactId>lesscss</artifactId>
							<version>1.7.0.1.2</version>
						</dependency>
					</dependencies>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-war-plugin</artifactId>
					<version>2.4</version>
					<configuration>
						<webResources>
							<!-- Instruct war plugin to include temp build directory in webapp -->
							<resource>
								<directory>${project.build.directory}/less-out</directory>
								<includes>
									<include>**/*</include>
								</includes>
							</resource>
						</webResources>
					</configuration>
				</plugin>
				<plugin>
					<groupId>com.mycila</groupId>
					<artifactId>license-maven-plugin</artifactId>
					<version>2.6</version>
					<configuration>
						<header>src/etc/header.txt</header>
						<properties>
							<year>${project.inceptionYear} - 2016</year>
							<description>
								${project.name} - ${project.description}
							</description>
						</properties>
						<excludes>
							<exclude>**/LICENSE*</exclude>
							<exclude>**/README*</exclude>
							<exclude>src/test/resources/**</exclude>
							<exclude>src/main/resources/**</exclude>
							<exclude>src/main/webapp/**</exclude>
							<exclude>src/main/assembly/**</exclude>
							<exclude>src/site/**</exclude>
							<exclude>misc/**</exclude>
						</excludes>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
</project>
